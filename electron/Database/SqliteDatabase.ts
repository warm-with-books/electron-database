import BetterSqlite3 from 'better-sqlite3';
import path from 'node:path';
export default class SqliteDatabase {
  private db: BetterSqlite3.Database;
  constructor() {
    this.db = new BetterSqlite3(path.join(__dirname,'music.db'));
  }

  // 检查表是否存在
  tableExists(tableName: string): boolean {
    const result = this.db.prepare(`SELECT name FROM sqlite_master WHERE type='table' AND name=?`).get(tableName);
    return !!result;
  }

  // 创建表
  createTableIfNotExists(tableName: string, tableDefinition: string): void {
    if (!this.tableExists(tableName)) {
      this.run(`CREATE TABLE ${tableName} (${tableDefinition})`);
    }
  }

  // 执行 SQL 查询，根据条件查询结果
  query(sql: string, params: unknown = {}): unknown[] {
    const statement = this.db.prepare(sql);
    return statement.all(params) as unknown[];
  }

  // 执行 SQL 查询，并返回第一行结果
  queryOne(sql: string, params: unknown = {}): unknown | undefined {
    const statement = this.db.prepare(sql);
    return statement.get(params) as unknown;
  }

  // 执行 SQL 命令，不返回结果
  run(sql: string, params: unknown = {}): BetterSqlite3.RunResult {
    const statement = this.db.prepare(sql);
    return statement.run(params) as BetterSqlite3.RunResult;
  }

  // 关闭数据库连接
  close(): void {
    this.db.close();
  }
}
