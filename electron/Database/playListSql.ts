import SqliteDatabase from "./SqliteDatabase";
export class playListSql {
  private db: SqliteDatabase;
  private tableName = "play_list"
  constructor() {
    this.db = new SqliteDatabase();
    this.db.createTableIfNotExists(
      this.tableName,
      "id INTEGER PRIMARY KEY AUTOINCREMENT, songId TEXT, title TEXT, artist TEXT,albumTitle TEXT, duration INTEGER"
    );
  }

  queryAll() {
    return this.db.query(`SELECT * FROM ${this.tableName}`);
  }

  queryById(songId: string) {
    return this.db.query(`SELECT * FROM ${this.tableName} WHERE songId = ?`, [songId])
  }

  deleteById(songId: string) {
    return this.db.run(`DELETE FROM ${this.tableName} WHERE songId = ?`, [songId])
  }

  deleteAll(){
    return this.db.run(`DELETE FROM ${this.tableName}`)
  }

  insert(parms:musicList){
    return this.db.run(`INSERT INTO ${this.tableName} (songId, title, artist, albumTitle, duration) VALUES (@songId, @title, @artist, @albumTitle, @duration)`,parms)
  }

  close(){
    this.db.close()
  }
}

